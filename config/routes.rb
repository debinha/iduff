Rails.application.routes.draw do
  resources :professors
  get '/login', to: 'session#login'
  post '/sign_up', to: 'register#sign_up'
  resources :userrs, except: [:create]
  resources :studens 
  resources :classes  
  get '/current_user', to: 'application#user_must_exist ' 
  resources :users do
    resources :subjects
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  end
end
