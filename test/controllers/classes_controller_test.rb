require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @classes = classes(:one)
  end

  test "should get index" do
    get comments_url, as: :json
    assert_response :success
  end

  test "should create classes" do
    assert_difference('Classes.count') do
      post classes_url, params: { classes: { message: @classes.message } }, as: :json
    end

    assert_response 201
  end

  test "should show classes" do
    get comment_url(@classes), as: :json
    assert_response :success
  end

  test "should update classes" do
    patch comment_url(@classes), params: { classes: { message: @classes.message } }, as: :json
    assert_response 200
  end

  test "should destroy classes" do
    assert_difference('Classes.count', -1) do
      delete classes_url(@classes), as: :json
    end

    assert_response 204
  end
end
