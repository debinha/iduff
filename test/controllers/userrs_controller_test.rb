require 'test_helper'

class UserrsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @userr = userrs(:one)
  end

  test "should get index" do
    get userrs_url, as: :json
    assert_response :success
  end

  test "should create userr" do
    assert_difference('Userr.count') do
      post userrs_url, params: { userr: { email: @userr.email, password_digest: @userr.password_digest } }, as: :json
    end

    assert_response 201
  end

  test "should show userr" do
    get userr_url(@userr), as: :json
    assert_response :success
  end

  test "should update userr" do
    patch userr_url(@userr), params: { userr: { email: @userr.email, password_digest: @userr.password_digest } }, as: :json
    assert_response 200
  end

  test "should destroy userr" do
    assert_difference('Userr.count', -1) do
      delete userr_url(@userr), as: :json
    end

    assert_response 204
  end
end
