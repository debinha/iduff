require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @post = posts(:one)
  end

  test "should get index" do
    get subjects_url, as: :json
    assert_response :success
  end

  test "should create post" do
    assert_difference('Post.count') do
      post subjects_url, params: { post: { message: @subject.message, title: @subject.title, user_id: @subject.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show subject" do
    get subject_url(@subject), as: :json
    assert_response :success
  end

  test "should update subject" do
    patch subject_url(@subject), params: { subject: { message: @subject.message, title: @subject.title, user_id: @subject.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy subjectt" do
    assert_difference('Subject.count', -1) do
      delete subject_url(@subject), as: :json
    end

    assert_response 204
  end
end
