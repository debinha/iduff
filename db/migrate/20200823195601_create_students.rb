class CreatesStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.references :likeable, polymorphic: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
