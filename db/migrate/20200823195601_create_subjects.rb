class CreateSubjects < ActiveRecord::Migration[5.2]
  def change
    create_table :subjects do |t|
      t.string :message
      t.references :user, foreign_key: true
      t.string :title

      t.timestamps
    end
  end
end
