class ClassesController < ApplicationController
  before_action :set_comment, only: [:show, :update, :destroy]

  # GET /classes
  def index
    @classes = Classes.all

    render json: @classes
  end

  # GET /classes/1
  def show
    render json: @classes
  end

  # POST /classes
  def create
    @classes = Comment.new(comment_params)

    if @classes.save
      render json: @classes, status: :created, location: @classes
    else
      render json: @classes.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /classes/1
  def update
    if @classes.update(comment_params)
      render json: @classes
    else
      render json: @classes.errors, status: :unprocessable_entity
    end
  end

  # DELETE /classes/1
  def destroy
    @classes.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @classes = Classes.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def comment_params
      params.require(:classes).permit(:message, :post_id, :user_id)
    end
end
