class StudentsController < ApplicationController
  before_action :set_like, only: [:show, :update, :destroy]

  # GET /students
  def index
    @students = Students.all

    render json: @students
  end

  # GET /students/1
  def show
    render json: @students
  end

  # POST /students
  def create
    @students = Students.new(students_params)

    if @students.save
      render json: @students, status: :created, location: @students
    else
      render json: @students.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /students/1
  def update
    if @students.update(students_params)
      render json: @students
    else
      render json: @students.errors, status: :unprocessable_entity
    end
  end

  # DELETE /students/1
  def destroy
    @students.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_like
      @students = Students.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def like_params
      params.require(:students).permit(:likeable_id, :likeable_type, :user_id)
    end
end
