class UserrsController < ApplicationController
  before_action :set_userr, only: [:show, :update, :destroy]
  before_action :must_be_sigining_in
  # GET /userrs
  def index
    @userrs = Userr.all

    render json: @userrs
  end

  # GET /userrs/1
  def show
    render json: @userr
  end

  # POST /userrs
  def create
    @userr = Userr.new(userr_params)

    if @userr.save
      render json: @userr, status: :created, location: @userr
    else
      render json: @userr.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /userrs/1
  def update
    if @userr.update(userr_params)
      render json: @userr
    else
      render json: @userr.errors, status: :unprocessable_entity
    end
  end

  # DELETE /userrs/1
  def destroy
    @userr.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_userr
      @userr = Userr.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def userr_params
      params.require(:userr).permit(:email, :password, :password_confirmation)
    end
end
