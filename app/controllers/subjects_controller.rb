class SubjectsController < ApplicationController
  before_action :set_post, only: [:show, :update, :destroy]

  # GET /subject
  def index
    @subject = Post.where(user_id: params[:user_id]).all

    render json: @subject
  end

  # GET /subject/1
  def show
    render json: @subject
  end

  # POST /subject
  def create
    @subject = Subject.new(subject_params)
    @subject.user_id = params[:user_id]
    if @subject.save
      render json: @subject, status: :created 
    else
      render json: @subject.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /subject/1
  def update
    if @subject.update(post_params)
      render json: @subject
    else
      render json: @subject.errors, status: :unprocessable_entity
    end
  end

  # DELETE /subject/1
  def destroy
    @subject.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @subject = Subject.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def post_params
      params.require(:subject).permit(:message, :user_id, :title)
    end
end
