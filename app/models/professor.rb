class Professor < ApplicationRecord
    has_many :subjects
    has_many :class
    has_many :students

    validates :name, :email, :birth, presence: true
    validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }, uniqueness: true
    validates :name, length: {minimum:1, maximum:100}
       
    has_many  :class_name => 'Classes', :foreign_key => :class_id
   
    has_many  :class_name => 'Classes', :foreign_key => :class_id
   
end
