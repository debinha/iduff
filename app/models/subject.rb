class Subject < ApplicationRecord
  belongs_to :user
  has_many :student
  has_many :class, as: :likeable
  validates :subject, presence: true,length: {minimum:1, maximum:1000}
end
