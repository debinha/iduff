class Classes < ApplicationRecord
    belongs_to :user
    belongs_to :subject
    has_many :student, as: :likeable
    validates :menssage, presence: true,length: {minimum:1, maximum:1000}
    
end
