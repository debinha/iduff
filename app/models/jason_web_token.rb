class JsonWebToken
    secret = ENV["DEVISE_JWT_SECRET_KEY"]
    def self.encode(payload)
        JWT.encode(payload, secret)
    end

    def self.decode3(token)
        begin
            decoded = JWT..decode(token, secret)
        rescue => exception
            return nil
        end
    end
end